namespace DepressionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Healths",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        HealthDate = c.DateTime(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        ConfirmPassword = c.String(),
                        LoginErrorMessage = c.String(),
                        Isverificated = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Moods",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Description = c.String(),
                        MoodDate = c.DateTime(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Relaxes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.Boolean(nullable: false),
                        RelaxDate = c.DateTime(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Relaxes", "User_ID", "dbo.Users");
            DropForeignKey("dbo.Moods", "User_ID", "dbo.Users");
            DropForeignKey("dbo.Healths", "User_ID", "dbo.Users");
            DropIndex("dbo.Relaxes", new[] { "User_ID" });
            DropIndex("dbo.Moods", new[] { "User_ID" });
            DropIndex("dbo.Healths", new[] { "User_ID" });
            DropTable("dbo.Relaxes");
            DropTable("dbo.Moods");
            DropTable("dbo.Users");
            DropTable("dbo.Healths");
        }
    }
}
