﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DepressionApp.Models;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DepressionApp.Controllers
{
    public class ChartController : Controller
    {
        // GET: Chart
        [HttpPost]
        public ActionResult Index()
        {

            using (DepressionAppContext db = new DepressionAppContext())
            {
                var list1 = db.Moods.Select(x =>  x.Value).ToList().Distinct();

                var list2 = db.Moods.Select(x => x.MoodDate).ToList().Distinct(); ;

                ViewBag.ValuesArray = JsonConvert.SerializeObject(list1);
                ViewBag.DateArray = JsonConvert.SerializeObject(list2);
                return RedirectToAction("Analysis", "Home");

            }
        }
        } 
}