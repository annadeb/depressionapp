﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.Encodings.Web;

namespace DepressionApp.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Mood()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}
		public ActionResult Health()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}
		public ActionResult Relax()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
		public ActionResult Login()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
		public ActionResult Register()
		{
			return View();
		}
		public ActionResult Analysis()
		{
			return View();
		}

		
		public ActionResult Animation()
		{
			return View();
		}
		//public ActionResult User()
		//{
		//	ViewBag.Message = "Your contact page.";

		//	return View();
		//}

		public string Welcome(string name, int numTimes = 1)
		{
			return HtmlEncoder.Default.Encode($"Hello {name}, NumTimes is: {numTimes}");
		}
	}
}