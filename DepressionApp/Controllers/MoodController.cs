﻿using DepressionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DepressionApp.Controllers
{
	public class MoodController : Controller
	{
		// GET: Mood
		public ActionResult Index()
		{
			return View();
		}

		
		public ActionResult SaveMood()
		{
			return View();
		}

		[HttpPost]
		public ActionResult SaveMood(DepressionApp.Models.Mood mood)
		{
			
			using (var context = new DepressionAppContext())
			{
				int id = (int)Session["ID"];
				User user = context.Users.Find(id);

				var moods = new List<Mood>
					{
					new Mood{Value=mood.Value,Description=mood.Description,MoodDate=DateTime.Today, User=user}
				};

				moods.ForEach(s => context.Moods.Add(s));
				context.SaveChanges();

			} 

			//return View("Mood", "Home");
			return RedirectToAction("Index", "Home");
		}
	}
}