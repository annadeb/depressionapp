﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DepressionApp.Models;
using System.Data;
using System.Data.Entity;
using System.Net;
//using DepressionApp.DAL;


namespace DepressionApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
		{

			//using (DepressionAppContext db = new DepressionAppContext())
			//{
			//	var users = new List<User>
			//		{
			//		new User{Email="Carson",Password="Alexander"},
			//		new User{Email="Meredith",Password="Alonso"},
			//		new User{Email="Arturo",Password="Anand"},
			//		new User{Email="Gytis",Password="Barzdukas"},
			//		new User{Email="Yan",Password="Li"},
			//		new User{Email="Peggy",Password="Justice"},
			//		new User{Email="Laura",Password="Norman"},
			//		new User{Email="Nino",Password="Olivetto"}
			//		};

			//	db.Users.Add(users[0]);
			//	db.SaveChanges();
			//}

				return View();
        }

        [HttpPost]
        public ActionResult Autherize(DepressionApp.Models.User userModel)
        {
            using (DepressionAppContext db = new DepressionAppContext())
            {
				userModel.Password = Crypto.Hash(userModel.Password);
				var userDetails = db.Users.Where(x => x.Email == userModel.Email && x.Password == userModel.Password).FirstOrDefault();
                if (userDetails == null)
                {
                    userModel.LoginErrorMessage = "Wrong username or password.";
                    return View("Index", userModel);
                    
                }
                else
                {
                    Session["ID"] = userDetails.ID;
                    Session["Email"] = userDetails.Email;
                    return RedirectToAction( "Index", "Home");
                }
            }
        }

        public ActionResult LogOut()
        {
			if (Session["ID"]!=null)
			{
				int userId = (int)Session["ID"];
				Session.Abandon();
			}
            return RedirectToAction("Login", "Home");
        }
    }
}