﻿using DepressionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DepressionApp.Controllers
{
    public class HealthController : Controller
    {
        // GET: Health
        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		public ActionResult SaveHealth(DepressionApp.Models.Health health)
		{

			using (var context = new DepressionAppContext())
			{
				int id = (int)Session["ID"];
				User user = context.Users.Find(id);

				var healths = new List<Health>
					{
					new Health{Value=health.Value, HealthDate=DateTime.Today, User=user}
				};

				healths.ForEach(s => context.Healths.Add(s));
				context.SaveChanges();

			}
			return RedirectToAction("Index", "Home");
		}
	}
}