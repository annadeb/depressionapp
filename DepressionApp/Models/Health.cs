﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DepressionApp.Models
{
	public class Health
	{
		[Key]
		public int ID { get; set; }
		//public string UserID { get; set; }
		public int Value { get; set; }
		public DateTime HealthDate { get; set; }

		public virtual User User { get; set; }
	}
}