﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DepressionApp.Models;
using System.Web.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace DepressionApp.Models
{
	public class DepressionAppContext : DbContext
	{
		public DepressionAppContext() : base("name=DepressionAppDb")
		{
		}
		public DbSet<User> Users { get; set; }
		public DbSet<Health> Healths { get; set; }
		public DbSet<Mood> Moods { get; set; }
		public DbSet<Relax> Relaxes { get; set; }
	}
}