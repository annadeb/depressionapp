﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DepressionApp.Models
{
	public class Relax
	{
		[Key]
		public int ID { get; set; }
		//public string UserID { get; set; }
		public bool Value { get; set; }
		public DateTime RelaxDate { get; set; }

		public virtual User User { get; set; }
	}
}