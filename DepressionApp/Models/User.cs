﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;


namespace DepressionApp.Models
{
	public class User
	{
        internal Guid ActivationCode;

        [Key]
		public int ID { get; set; }
        [DisplayName("User Name")] //nowe
        [Required(ErrorMessage = "This field is required.")]//nowe
        public string Email { get; set; }
        
        [DataType(DataType.Password)]//nowe
        [Required(ErrorMessage = "This field is required.")]//nowe
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        //public DateTime EnrollmentDate { get; set; }

        //public virtual ICollection<Enrollment> Enrollments { get; set; }

        public string LoginErrorMessage { get; set; }//nowe

        public bool Isverificated { get; set; } // NOWE BARDZO
    }
}